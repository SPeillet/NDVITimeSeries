/*=========================================================================

  Program:   NDVITimeSeries
  Language:  C++

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.

  See LICENSE for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef __otbNDVITimeSeriesFunctor_h
#define __otbNDVITimeSeriesFunctor_h

#include "itkNumericTraits.h"
#include "vnl/vnl_vector.h"
#include <vector>
#include "otbDates.h"
#include <numeric>

namespace otb
{
namespace functor
{

/*
 * \class TFunctorWithDates
 *
 * \brief Base class for functors which use input dates
 *
 * \ingroup NDVITimeSeries
 */
template<class TDateType>
class TFunctorWithDates
{
public:
  typedef typename std::vector<TDateType> DatesType;

  TFunctorWithDates() {}
  ~TFunctorWithDates() {}
  bool operator!=( const TFunctorWithDates & ) const { return false; }
  bool operator==( const TFunctorWithDates & other ) const { return !(*this != other); }

  // Set dates
  void SetDates(DatesType dates)
  {
    unsigned int n = dates.size();

    // Check that dates are ordered in chronological
    for (unsigned int i = 0 ; i < n - 1 ; i++)
      {
      assert(dates[i].julianday < dates[i+1].julianday);
      }

    // Assign dates
    m_Dates = dates;

  }

  // Get dates
  DatesType GetDates() const { return m_Dates; }

private:
  DatesType m_Dates;
}; // TFunctorWithDates

/**
 * \class TSReduceFunctorBase
 *
 * \brief Base class for reducing N-components pixel into M-component pixel (N=nb. of images and M=nb. of years)
 *
 * Dates must be provided in chronological order, else the "SetDates()" method will throw an exception.
 *
 * \ingroup NDVITimeSeries
 */
template< class TInputPixel, class TOutputPixel>
class TSReduceFunctorBase: public TFunctorWithDates<otb::dates::SingleDate>
{
public:
  typedef otb::dates::SingleDate DateType;
  typedef std::vector<DateType> DatesType;
  typedef typename TInputPixel::ValueType PixelValueType;

  TSReduceFunctorBase() {}
  ~TSReduceFunctorBase() {}
  bool operator!=( const TSReduceFunctorBase & ) const { return false; }
  bool operator==( const TSReduceFunctorBase & other ) const { return !(*this != other); }
  virtual inline TOutputPixel operator()( const TInputPixel & A ) const = 0;

  inline unsigned int GetOutputSize() const { return nbOfYears; }

  constexpr std::size_t OutputSize(std::array<long unsigned int, 1ul>&) const
  {
    return nbOfYears;
  }
  // Set / Get input no-data value
  void SetInputNoDataValue(PixelValueType value) { m_InputNoDataValue = value; }
  PixelValueType GetInputNoDataValue() const { return m_InputNoDataValue; }

  // Set dates
  void SetDates(DatesType dates)
  {
    TFunctorWithDates<otb::dates::SingleDate>::SetDates(dates);

    // Compute the number of years
    unsigned int n = GetDates().size();
    nbOfYears = GetDates()[n-1].year - GetDates()[0].year + 1;
  }

private:
  unsigned int nbOfYears;
  PixelValueType m_InputNoDataValue;

}; // TSReduceFunctorBase


/**
 * \class TSCumulatedRangeReduceFunctor
 *
 * \brief Class for reducing N-components pixel into M-component pixel (N=nb. of images and M=nb. of years)
 * given a range (dd/mm--->dd'/mm')
 *
 * \ingroup NDVITimeSeries
 */
template< class TInputPixel, class TOutputPixel>
class TSCumulatedRangeReduceFunctor : public TSReduceFunctorBase<TInputPixel,TOutputPixel>
{
public:

typedef typename TSReduceFunctorBase<TInputPixel,TOutputPixel>::DatesType DatesType;
typedef typename TSReduceFunctorBase<TInputPixel,TOutputPixel>::DateType DateType;
typedef typename TInputPixel::ValueType PixelValueType;

TSCumulatedRangeReduceFunctor() {}

~TSCumulatedRangeReduceFunctor() {}

bool operator!=( const TSCumulatedRangeReduceFunctor & ) const {
  return false;
}

bool operator==( const TSCumulatedRangeReduceFunctor & other ) const {
  return !(*this != other);
}

inline TOutputPixel operator ()(const TInputPixel& input) const
{
  TOutputPixel output;
  output.SetSize(this->GetOutputSize());

  DatesType dates = this->GetDates();
  int firstYear = dates[0].year;

  for (unsigned int k = 0 ; k < this->GetOutputSize() ; k++)
    {
    DateType startDate(firstYear + k, m_Month, m_Day);
    output[k] = 0;
    for (unsigned int i = 0 ; i < input.Size() ; i++)
      {
      PixelValueType inValue = input[i];
      if (inValue != this->GetInputNoDataValue() && dates[i].IsInRange(startDate, m_NbOfDays))
        {
        output[k] += inValue;
        }
      }
    }

  return output;
}

void SetStartDay(int day) { m_Day = day; }
void SetStartMonth(int month) { m_Month = month; }
void SetNbOfDays(int n) { m_NbOfDays = n; }

private:
int m_Day;
int m_Month;
int m_NbOfDays;

}; // TSCumulatedRangeReduceFunctor

/**
 * \class TSMaxReduceFunctor
 *
 * \brief Class for reducing N-components pixel into M-component pixel (N=nb. of images and M=nb. of years)
 * returns for each year the max() value
 *
 * \ingroup NDVITimeSeries
 */
template< class TInputPixel, class TOutputPixel>
class TSMaxReduceFunctor : public TSReduceFunctorBase<TInputPixel,TOutputPixel>
{
public:

typedef typename TSReduceFunctorBase<TInputPixel,TOutputPixel>::DatesType DatesType;
typedef typename TInputPixel::ValueType PixelValueType;

TSMaxReduceFunctor() {}

~TSMaxReduceFunctor() {}

bool operator!=( const TSMaxReduceFunctor & ) const {
  return false;
}

bool operator==( const TSMaxReduceFunctor & other ) const {
  return !(*this != other);
}

inline TOutputPixel operator ()(const TInputPixel& input) const
{
  TOutputPixel output;
  output.SetSize(this->GetOutputSize());
  output.Fill(this->GetInputNoDataValue());

  DatesType dates = this->GetDates();

  int firstYear = dates[0].year;
  for (unsigned int i = 0; i < input.Size(); i++)
    {
    int index = dates[i].year - firstYear;
    PixelValueType inValue = input[i];

    if (inValue != this->GetInputNoDataValue())
      {
      if (output[index] == this->GetInputNoDataValue())
        {
        output[index] = inValue;
        }
      else if (output[index] < inValue)
        {
        output[index] = inValue;
        }
      }
    }
  return output;
}

}; // TSMaxReduceFunctor

/**
 * \class TSAmplitudeReduceFunctor
 *
 * \brief Class for reducing N-components pixel into M-component pixel (N=nb. of images and M=nb. of years)
 * returns for each year the max()-min() value
 *
 * \ingroup NDVITimeSeries
 */
template< class TInputPixel, class TOutputPixel>
class TSAmplitudeReduceFunctor : public TSReduceFunctorBase<TInputPixel,TOutputPixel>
{
public:

typedef typename TSReduceFunctorBase<TInputPixel,TOutputPixel>::DatesType DatesType;
typedef typename TInputPixel::ValueType PixelValueType;

TSAmplitudeReduceFunctor() {}

~TSAmplitudeReduceFunctor() {}

bool operator!=( const TSAmplitudeReduceFunctor & ) const {
  return false;
}

bool operator==( const TSAmplitudeReduceFunctor & other ) const {
  return !(*this != other);
}

inline TOutputPixel operator ()(const TInputPixel& input) const
{
  TOutputPixel output;
  output.SetSize(this->GetOutputSize());
  output.Fill(this->GetInputNoDataValue());

  DatesType dates = this->GetDates();
  TOutputPixel minimums, maximums;
  minimums.SetSize(this->GetOutputSize());
  maximums.SetSize(this->GetOutputSize());
  minimums.Fill(this->GetInputNoDataValue());
  maximums.Fill(this->GetInputNoDataValue());

  int firstYear = dates[0].year;
  for (unsigned int i = 0; i < input.Size(); i++)
    {
    int index = dates[i].year - firstYear;
    PixelValueType inValue = input[i];

    if (inValue != this->GetInputNoDataValue())
      {
      // Update maximums
      if (maximums[index] == this->GetInputNoDataValue())
        {
        maximums[index] = inValue;
        }
      else if (maximums[index] < inValue)
        {
        maximums[index] = inValue;
        }

      // Update minimums
      if (minimums[index] == this->GetInputNoDataValue())
        {
        minimums[index] = inValue;
        }
      else if (inValue < minimums[index])
        {
        minimums[index] = inValue;
        }
      }
    }

  // Compute amplitude
  int lastYear = dates[dates.size()-1].year;
  for (int i = 0 ; i < lastYear - firstYear +1 ; i++ )
    {
    if (minimums[i] != this->GetInputNoDataValue() && maximums[i] != this->GetInputNoDataValue())
      {
      output[i] = maximums[i] - minimums[i];
      }
    }

  return output;
}

}; // TSAmplitudeReduceFunctor

/**
 * \class SlopeAndPValueFunctor
 *
 * \brief Class for computing the slope and the p-value from an input sample (pixel)
 *
 * The functor returns an output pixel of dimension 3 with:
 *   outputPixel[0] : slope (ordinary least square regression)
 *   outputPixel[1] : p-value (student coefficient)
 *
 */
template< class TInputPixel, class TOutputPixel>
class SlopeAndPValueFunctor: public TFunctorWithDates<otb::dates::SingleDate>
{

public:
  typedef typename TInputPixel::ValueType InputPixelValueType;
  typedef typename TOutputPixel::ValueType OutputPixelValueType;
  typedef otb::dates::SingleDate DateType;
  typedef std::vector<DateType> DatesType;

  // Constructor
  SlopeAndPValueFunctor()
  {
    /*
     * The number of components for the output pixel is 2 (slope, p-value)
     */
    numberOfComponentsPerPixel = 2;
  }

  // Destructor
  ~SlopeAndPValueFunctor(){}

  // Purposely not implemented
  bool operator!=( const SlopeAndPValueFunctor & ) const { return false; }
  bool operator==( const SlopeAndPValueFunctor & other ) const { return !(*this != other); }

  // Get output size
  inline unsigned int GetOutputSize(){ return numberOfComponentsPerPixel; }

  static constexpr std::size_t outputPixelSize{1};

  std::size_t OutputSize(std::array<long unsigned int, 1ul>&) const
  {
    return numberOfComponentsPerPixel;
  }

  // Compute output pixel
  inline TOutputPixel operator ()(const TInputPixel& input) const
  {
    const int firstDate = GetDates()[0].julianday;

    TOutputPixel outpixel;
    outpixel.SetSize(numberOfComponentsPerPixel);
    outpixel.Fill(m_OutputNoDataValue);

    // compute sum & sum of squares
    unsigned int n = input.Size();
    OutputPixelValueType sum = 0;
    OutputPixelValueType isum = 0;
    OutputPixelValueType sqSum = 0;
    OutputPixelValueType isqSum = 0;
    OutputPixelValueType coSum = 0;
    unsigned int count = 0;
    for (unsigned int i = 0 ; i < n ; i++)
      {
      if (input[i] != m_InputNoDataValue)
        {
        OutputPixelValueType val = static_cast<OutputPixelValueType>(input[i]);
        OutputPixelValueType ival = static_cast<OutputPixelValueType>(GetDates()[i].julianday - firstDate);
        sum += val;
        isum += ival;
        sqSum += val*val;
        isqSum += ival*ival;
        coSum += val*ival;
        count++;
        }
      }

    if (count > 1)
      {
      OutputPixelValueType nf = static_cast<OutputPixelValueType>(count);
      OutputPixelValueType nfInv = -1.0 / nf;
      OutputPixelValueType sum2 = sum * sum;
      OutputPixelValueType isum2 = isum * isum;
      OutputPixelValueType sumisum = sum * isum;

      // Covariance
      OutputPixelValueType cov = coSum + nfInv * sumisum;

      // Variance(t)
      OutputPixelValueType var_t = isqSum + nfInv * isum2;

      // Variance(x)
      OutputPixelValueType var_x = sqSum + nfInv * sum2;

      // OLS
      OutputPixelValueType slope = cov / var_t;

      // Pearson linear correlation coefficient
      OutputPixelValueType correlation = cov / vcl_sqrt(var_t * var_x);

      // pvalue
      OutputPixelValueType pvalue = 0.0;
      if (vcl_abs(correlation) < 1 && nf > 2) // Degree of freedom > 0
        {
        // degrees of freedom
        OutputPixelValueType df = nf - 2;
        OutputPixelValueType t = correlation;
        t /= sqrt((1.0 - correlation * correlation) / df);

        // Compute student cumulative distribution function
        boost::math::students_t_distribution<OutputPixelValueType> dist(df);
        OutputPixelValueType cdf = boost::math::cdf(dist, vcl_abs(t));

        // Compute p-value
        pvalue = 2.0 * (1.0 - cdf);
        }

      // output pixel
      outpixel[0] = slope;
      outpixel[1] = pvalue;
      }

    return outpixel;
  }

  /* Set / Get input no-data value */
  void SetInputNoDataValue(InputPixelValueType value) { m_InputNoDataValue = value; }
  InputPixelValueType GetInputNoDataValue() const { return m_InputNoDataValue; }

  /* Set / Get output no-data value */
  void SetOutputNoDataValue(OutputPixelValueType value) { m_OutputNoDataValue = value; }
  OutputPixelValueType GetOutputNoDataValue() const { return m_OutputNoDataValue; }

protected:
  InputPixelValueType m_InputNoDataValue;
  OutputPixelValueType m_OutputNoDataValue;
  unsigned int numberOfComponentsPerPixel;
}; // SlopeAndPValueFunctor

/**
 * \class SlopeAndPValueLabelingFunctor
 *
 * \brief Class for computing the label corresponding to an input slope and p-value
 *
 *     Output value    |   Definition
 * --------------------------------------
 *           0         | No Data
 *           1         | Very Strong Neg.
 *           2         | Strong Neg.
 *           3         | Moderate Neg.
 *           4         | Very Strong Pos.
 *           5         | Strong Pos.
 *           6         | Moderate Pos.
 *           7         | No Significant
 *
 **/
template< class TInputPixel, class TLabelPixel>
class SlopeAndPValueLabelingFunctor
{
public:
  enum LabelValue
  {
    NoData,
    VeryStrongNeg,
    StrongNeg,
    ModerateNeg,
    VeryStrongPos,
    StrongPos,
    ModeratePos,
    NotSignificant
  };

  typedef typename TInputPixel::ValueType InputPixelValueType;
  typedef typename TLabelPixel::ValueType LabelPixelValueType;

  // Constructor
  SlopeAndPValueLabelingFunctor()
  {
    numberOfComponentsPerPixel = 1;
    m_InputNoDataValue = 0;
  }

  // Destructor
  ~SlopeAndPValueLabelingFunctor(){}

  // Purposely not implemented
  bool operator!=( const SlopeAndPValueLabelingFunctor & ) const { return false; }
  bool operator==( const SlopeAndPValueLabelingFunctor & other ) const { return !(*this != other); }

  // Get output size
  inline unsigned int GetOutputSize(){ return numberOfComponentsPerPixel; }

  static constexpr std::size_t outputPixelSize{1};

  std::size_t OutputSize(std::array<long unsigned int, 1ul>&) const
  {
    return numberOfComponentsPerPixel;
  }

  // Compute output pixel
  inline TLabelPixel operator ()(const TInputPixel& input) const
  {
    // Prepare output pixel
    TLabelPixel label;
    label.SetSize(numberOfComponentsPerPixel);

    // Get slope and p-value
    InputPixelValueType slope = input[0];
    InputPixelValueType pvalue = input[1];

    LabelValue outputLabelValue = NoData;
    if (slope != m_InputNoDataValue && pvalue != m_InputNoDataValue)
      {
      outputLabelValue = NotSignificant;
      if (slope < 0)
        {
        if (pvalue <= 0.001)
          {
          outputLabelValue = VeryStrongNeg;
          }
        else if (0.001 < pvalue && pvalue <= 0.01)
          {
          outputLabelValue = StrongNeg;
          }
        else if (0.01 < pvalue && pvalue <= 0.05)
          {
          outputLabelValue = ModerateNeg;
          }
        } // Slope < 0
      else if (slope > 0)
        {
        if (pvalue <= 0.001)
          {
          outputLabelValue = VeryStrongPos;
          }
        else if (0.001 < pvalue && pvalue <= 0.01)
          {
          outputLabelValue = StrongPos;
          }
        else if (0.01 < pvalue && pvalue <= 0.05)
          {
          outputLabelValue = ModeratePos;
          }
        } // Slope > 0
      } // Value is different than no-data

    label[0] = static_cast<LabelPixelValueType>(outputLabelValue);

    return label;
  }

  /* Set / Get input no-data value */
  void SetInputNoDataValue(InputPixelValueType value) { m_InputNoDataValue = value; }
  InputPixelValueType GetInputNoDataValue() const { return m_InputNoDataValue; }

protected:
  InputPixelValueType m_InputNoDataValue;

  unsigned int numberOfComponentsPerPixel;

}; // SlopeAndPValueLabelingFunctor

/*
 * \class RainfallEstimatedNDVIResiduesFunctor
 * \brief Compute the residues between the estimation of the NDVI from the rainfall,
 * and the real NDVI. Returns the residues.
 */
template< class TNDVIPixel, class TRainfallPixel>
class RainfallEstimatedNDVIResiduesFunctor
{
public:
  typedef typename TNDVIPixel::ValueType NDVIPixelValueType;
  typedef typename TRainfallPixel::ValueType RainfallPixelValueType;

  RainfallEstimatedNDVIResiduesFunctor(){
    m_NDVINoDataValue = 0;
    m_RainfallNoDataValue = 0;
  }
  ~RainfallEstimatedNDVIResiduesFunctor(){}

  std::size_t OutputSize(std::array<long unsigned int, 2ul> Inputs) const
  {
    unsigned int nbComp = std::accumulate(Inputs.begin(),Inputs.end(),0)/2;
    return nbComp;
  }

  // Purposely not implemented
  bool operator!=( const RainfallEstimatedNDVIResiduesFunctor & ) const { return false; }
  bool operator==( const RainfallEstimatedNDVIResiduesFunctor & other ) const { return !(*this != other); }

  /* Set / Get NDVI input no-data value */
  void SetNDVINoDataValue(NDVIPixelValueType value) { m_NDVINoDataValue = value; }
  NDVIPixelValueType GetNDVINoDataValue() const { return m_NDVINoDataValue; }

  /* Set / Get Rainfall input no-data value */
  void SetRainfallNoDataValue(RainfallPixelValueType value) { m_RainfallNoDataValue = value; }
  RainfallPixelValueType GetRainfallNoDataValue() const { return m_RainfallNoDataValue; }

  /*
   * Compute output pixel.
   * inputNDVIPixel and inputRainfallPixel must have the same number of components.
   */
  inline TNDVIPixel operator ()(const TNDVIPixel& inputNDVIPixel, const TRainfallPixel& inputRainfallPixel) const
  {
    unsigned int n = inputNDVIPixel.Size();

//    // TODO: delete this ////
//        std::cout << "Operator ()" << std::endl;
//        std::cout << "Input pixels:" << std::endl;
//        std::cout << "ndvi=[";
//        for (unsigned int i = 0 ; i < n ; i++)
//          {
//          if (inputNDVIPixel[i] != m_NDVINoDataValue && inputRainfallPixel[i] != m_RainfallNoDataValue)
//            {
//            std::cout << inputNDVIPixel[i] << ", ";
//            }
//          }
//        std::cout << "];" << std::endl;
//        std::cout << "rain=[";
//        for (unsigned int i = 0 ; i < n ; i++)
//          {
//          if (inputNDVIPixel[i] != m_NDVINoDataValue && inputRainfallPixel[i] != m_RainfallNoDataValue)
//            {
//            std::cout << inputRainfallPixel[i] << ", ";
//            }
//          }
//        std::cout << "];" << std::endl;
//    /////////////////////////

    // Prepare output pixel
    TNDVIPixel residues;
    residues.SetSize(n);

    // 1. Compute regression between Y=NDVI and X=Rainfall
    NDVIPixelValueType ndviSum = 0;
    NDVIPixelValueType rainSum = 0;
    NDVIPixelValueType rainSqSum = 0;
    NDVIPixelValueType coSum = 0;
    unsigned int count = 0;
    for (unsigned int i = 0 ; i < n ; i++)
      {
      if (inputNDVIPixel[i] != m_NDVINoDataValue && inputRainfallPixel[i] != m_RainfallNoDataValue)
        {
        NDVIPixelValueType ndviVal = inputNDVIPixel[i];
        NDVIPixelValueType rainVal = static_cast<NDVIPixelValueType>(inputRainfallPixel[i]);
        ndviSum += ndviVal;
        rainSum += rainVal;
        rainSqSum += rainVal*rainVal;
        coSum += ndviVal*rainVal;
        count++;
        }
      }

    if (count > 1)
      {
      NDVIPixelValueType nf = static_cast<NDVIPixelValueType>(count);
      NDVIPixelValueType nfInv = -1.0 / nf;
      NDVIPixelValueType rainSum2 = rainSum * rainSum;
      NDVIPixelValueType prodsums = ndviSum * rainSum;

      // Covariance
      NDVIPixelValueType cov = coSum + nfInv * prodsums;

      // Variance (rain)
      NDVIPixelValueType var_rain = rainSqSum + nfInv * rainSum2;

      // OLS Y = slope * X + zeroy
      NDVIPixelValueType slope = cov / var_rain;
      NDVIPixelValueType zeroy = nfInv * ( slope * rainSum - ndviSum );

      // 2. Compute residues
      for (unsigned int i = 0 ; i < n ; i++)
        {
        if (inputNDVIPixel[i] != m_NDVINoDataValue && inputRainfallPixel[i] != m_RainfallNoDataValue)
          {
          residues[i] = inputNDVIPixel[i] - (zeroy + slope * inputRainfallPixel[i]);
          }
        else
          {
          residues[i] = m_NDVINoDataValue;
          }
        }

      }
    else
      {
      residues.Fill(m_NDVINoDataValue);
      }

    // TODO: delete this
//    std::cout << "res=[";
//    for (unsigned int i = 0 ; i < n ; i++)
//      {
//      if (inputNDVIPixel[i] != m_NDVINoDataValue && inputRainfallPixel[i] != m_RainfallNoDataValue)
//        {
//        std::cout << residues[i] << ", ";
//        }
//      }
//    std::cout << "];" << std::endl;

    return residues;
  }
  // void SetStartDay(const TNDVIPixel& inputNDVIPixel) { numberOfComponentsPerPixel = inputNDVIPixel.Size(); }
protected:
  NDVIPixelValueType       m_NDVINoDataValue;
  RainfallPixelValueType   m_RainfallNoDataValue;
}; // RainfallEstimatedNDVIResiduesFunctor

/**
 * \class FactorsLabelingFunctor
 *
 * \brief Class for computing the label corresponding to the factor.
 *
 * Input:
 * -NDVI Trend (2-bands image: slope and p-value)
 * -Rainfall Trend (2-bands image: slope and p-value)
 * -NDVI vs Rainfall Correlation coefficient (1-band image)
 *
 * Output:
 * -Label (1-band image)
 *
 *     Output value    |   Definition
 * ----------------------------------------------
 *           0         | No data
 *           1         | Increase (Climate+Other)
 *           2         | Increase (Climate)
 *           3         | Increase (Other)
 *           4         | Decrease (Climate+Other)
 *           5         | Decrease (Climate)
 *           6         | Decrease (Other)
 *           7         | Not significant
 *
 **/
template< class TInputPixel, class TLabelPixel>
class FactorsLabelingFunctor
{
public:
  enum Labels
  {
    NoData,
    IncreaseBothClimateAndOther,
    IncreaseClimate,
    IncreaseOther,
    DecreaseBothClimateAndOther,
    DecreaseClimate,
    DecreaseOther,
    NotSignificant
  };

  typedef typename TInputPixel::ValueType InputPixelValueType;
  typedef typename TLabelPixel::ValueType LabelPixelValueType;

  // Constructor
  FactorsLabelingFunctor()
  {
    numberOfComponentsPerPixel = 1;
    m_InputNoDataValue = 0;
  }

  // Destructor
  ~FactorsLabelingFunctor(){}

  // Purposely not implemented
  bool operator!=( const FactorsLabelingFunctor & ) const { return false; }
  bool operator==( const FactorsLabelingFunctor & other ) const { return !(*this != other); }

  // Get output size
  inline unsigned int GetOutputSize(){ return numberOfComponentsPerPixel; }

  static constexpr std::size_t outputPixelSize{1};

  std::size_t OutputSize(std::array<long unsigned int, 3ul>&) const
  {
    return numberOfComponentsPerPixel;
  }

  // Compute output pixel
  inline TLabelPixel operator ()(const TInputPixel& ndviTrend, const TInputPixel& rainTrend, const TInputPixel& corr) const
  {
    // Prepare output pixel
    TLabelPixel label;
    label.SetSize(numberOfComponentsPerPixel);

    // Thresholds
    const InputPixelValueType ndviPValThresh = 0.05;
    const InputPixelValueType rfPValThresh = 0.05;
    const InputPixelValueType corrThresh = 0.5;

    // Get values
    InputPixelValueType ndviTrendSlope = ndviTrend[0];
    InputPixelValueType ndviTrendPValue = ndviTrend[1];
    InputPixelValueType rainTrendSlope = rainTrend[0];
    InputPixelValueType rainTrendPValue = rainTrend[1];
    InputPixelValueType correlation = corr[0];

    // Default label value
    Labels outputLabelValue = NoData;
    if (ndviTrendSlope != m_InputNoDataValue && ndviTrendPValue != m_InputNoDataValue &&
        rainTrendSlope != m_InputNoDataValue && rainTrendPValue != m_InputNoDataValue)
      {

      // Null slope, strong pvalue, ...
      outputLabelValue = NotSignificant;

      if (ndviTrendPValue < ndviPValThresh)
        {
        bool corrIsPos = (correlation > corrThresh);
        bool rtSlopeIsPos = (rainTrendSlope > 0);
        bool rtPValueCond = (rainTrendPValue < rfPValThresh);

        // Increase
        if (ndviTrendSlope > 0)
          {
          if (corrIsPos)
            {
            if (rtSlopeIsPos && rtPValueCond)
              {
              outputLabelValue = IncreaseBothClimateAndOther;
              }
            else if ((!rtSlopeIsPos && rtPValueCond) || !rtPValueCond)
              {
              outputLabelValue = IncreaseClimate;
              }
            } // correlation > corrThresh
          else
            {
            if (rtPValueCond || (!rtSlopeIsPos && !rtPValueCond))
              {
              outputLabelValue = IncreaseOther;
              }
            } // correlation <= corrThresh
          } // NDVI slope > 0
        else if (ndviTrendSlope < 0)
          {
          // Decrease
          if (corrIsPos)
            {
            if (rtSlopeIsPos)
              {
              outputLabelValue = DecreaseClimate;
              }
            else if (!rtSlopeIsPos && rtPValueCond)
              {
              outputLabelValue = DecreaseBothClimateAndOther;
              }
            } // correlation > corrThresh
          else
            {
            if (rtSlopeIsPos || (!rtSlopeIsPos && rtPValueCond))
              {
              outputLabelValue = DecreaseOther;
              }
            } // correlation <= corrThresh
          } // NDVI slope < 0
        } // NDVI p-value < 0.05
      } // Different than no-data

    label[0] = static_cast<LabelPixelValueType>(outputLabelValue);

    return label;
  }

  // Set / Get input no-data value
  void SetInputNoDataValue(InputPixelValueType value) { m_InputNoDataValue = value; }
  InputPixelValueType GetInputNoDataValue() const { return m_InputNoDataValue; }

protected:
  InputPixelValueType m_InputNoDataValue;

  unsigned int numberOfComponentsPerPixel;

}; // FactorsLabelingFunctor

/*
 * \class PearsonCorrelationCoefficientFunctor
 * \brief Computes the Pearson Correlation Coefficient
 *
 */
template< class TNDVIPixel, class TRainfallPixel, class TCorrelationPixel>
class PearsonCorrelationCoefficientFunctor
{
public:
  typedef typename TNDVIPixel::ValueType NDVIPixelValueType;
  typedef typename TRainfallPixel::ValueType RainfallPixelValueType;
  typedef typename TCorrelationPixel::ValueType CorrelationPixelValueType;

  PearsonCorrelationCoefficientFunctor(){
    m_CorrelationNoDataValue = itk::NumericTraits<CorrelationPixelValueType>::NonpositiveMin();
    numberOfComponentsPerPixel = 1;
  }
  ~PearsonCorrelationCoefficientFunctor(){}

  unsigned int GetOutputSize(){ return numberOfComponentsPerPixel; }

  static constexpr std::size_t outputPixelSize{1};

  std::size_t OutputSize(std::array<long unsigned int, 2ul>&) const
  {
    return numberOfComponentsPerPixel;
  }

  // Purposely not implemented
  bool operator!=( const PearsonCorrelationCoefficientFunctor & ) const { return false; }
  bool operator==( const PearsonCorrelationCoefficientFunctor & other ) const { return !(*this != other); }

  /* Set / Get NDVI input no-data value */
  void SetNDVINoDataValue(NDVIPixelValueType value) { m_NDVINoDataValue = value; }
  NDVIPixelValueType GetNDVINoDataValue() const { return m_NDVINoDataValue; }

  /* Set / Get Rainfall input no-data value */
  void SetRainfallNoDataValue(RainfallPixelValueType value) { m_RainfallNoDataValue = value; }
  RainfallPixelValueType GetRainfallNoDataValue() const { return m_RainfallNoDataValue; }

  /* Set / Get correlation input no-data value */
  void SetCorrelationNoDataValue(CorrelationPixelValueType value) { m_CorrelationNoDataValue = value; }
  CorrelationPixelValueType GetCorrelationNoDataValue() const { return m_CorrelationNoDataValue; }

  /*
   * Compute output pixel.
   * inputNDVIPixel and inputRainfallPixel must have the same number of components.
   */
  inline TCorrelationPixel operator ()(const TNDVIPixel& inputNDVIPixel, const TRainfallPixel& inputRainfallPixel) const
  {
    unsigned int n = inputNDVIPixel.Size();

    // Prepare output pixel
    TCorrelationPixel correlation;
    correlation.SetSize(n);

    // 1. Compute regression between Y=NDVI and X=Rainfall
    NDVIPixelValueType ndviSum = 0;
    NDVIPixelValueType rainSum = 0;
    NDVIPixelValueType ndviSqSum = 0;
    NDVIPixelValueType rainSqSum = 0;
    NDVIPixelValueType coSum = 0;
    unsigned int count = 0;
    for (unsigned int i = 0 ; i < n ; i++)
      {
      if (inputNDVIPixel[i] != m_NDVINoDataValue && inputRainfallPixel[i] != m_RainfallNoDataValue)
        {
        NDVIPixelValueType ndviVal = inputNDVIPixel[i];
        NDVIPixelValueType rainVal = static_cast<NDVIPixelValueType>(inputRainfallPixel[i]);
        ndviSum += ndviVal;
        rainSum += rainVal;
        ndviSqSum += ndviVal*ndviVal;
        rainSqSum += rainVal*rainVal;
        coSum += ndviVal*rainVal;
        count++;
        }
      }

    if (count > 1)
      {
      NDVIPixelValueType nf = static_cast<NDVIPixelValueType>(count);
      NDVIPixelValueType nfInv = -1.0 / nf;
      NDVIPixelValueType ndviSum2 = ndviSum * ndviSum;
      NDVIPixelValueType rainSum2 = rainSum * rainSum;
      NDVIPixelValueType prodsums = ndviSum * rainSum;

      // Covariance
      NDVIPixelValueType cov = coSum + nfInv * prodsums;

      // Variance(ndvi)
      NDVIPixelValueType var_ndvi = ndviSqSum + nfInv * ndviSum2;

      // Variance(rain)
      NDVIPixelValueType var_rain = rainSqSum + nfInv * rainSum2;

      // Pearson correlation coefficient
      correlation[0] = cov / vcl_sqrt(var_ndvi * var_rain);
      }
    else
      {
      correlation[0] = m_CorrelationNoDataValue;
      }

    return correlation;
  }

protected:
  NDVIPixelValueType       m_NDVINoDataValue;
  RainfallPixelValueType   m_RainfallNoDataValue;
  CorrelationPixelValueType m_CorrelationNoDataValue;

  unsigned int numberOfComponentsPerPixel;
}; // PearsonCorrelationCoefficientFunctor

} // namespace functor

} // namespace otb

#endif
