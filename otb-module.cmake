set(DOCUMENTATION "NDVI Time Series applications")

otb_module(NDVITimeSeries
  DEPENDS
    OTBITK
    OTBCommon
    OTBApplicationEngine
    TimeSeriesUtils
    OTBTemporalGapFilling
    LSGRM
    	
  TEST_DEPENDS
    OTBTestKernel
    OTBCommandLine
  DESCRIPTION
    $DOCUMENTATION
)
