# NDVI Time Series applications

This module contains two applications for NDVI time series processing.

## Dependencies

* The [OTB](https://www.orfeo-toolbox.org/) library
* The [TimeSeriesUtils](https://gitlab.irstea.fr/remi.cresson/TimeSeriesUtils) remote module for OTB
* The [TemporalSmoothing](https://gitlab.irstea.fr/remi.cresson/TemporalSmoothing) remote module for OTB
* The [TemporalGapFilling](http://tully.ups-tlse.fr/jordi/temporalgapfilling) remote module for OTB

## How to build it

The module can be built like any other [otb remote module](https://wiki.orfeo-toolbox.org/index.php/How_to_write_a_remote_module). You can build it either from within OTB's sources or outside it.

## TimeSeriesIndexTrend application

### Description

This application computes the trend of a given time series index (e.g. NDVI).
The implemented pipeline is described in [Leroux et al](https://doi.org/10.1016/j.rse.2017.01.014).

![Pipeline](otbTimeSeriesIndexTrend.png)


### How to use it

TimeSeriesIndexTrend is an OTBApplication.

```
This is the TimeSeriesIndexTrend (TimeSeriesIndexTrend) application, version 6.4.0

Compute the trend of the vegetation index time series
Parameters: 
MISSING -ndvits                   <string list>    Input NDVI time series images  (mandatory)
MISSING -ndvidates                <string>         Input NDVI time series dates ASCII file (Must be YYYYMMDD format)  (mandatory)
        -rainfts                  <string list>    Input rainfall time series images  (optional, off by default)
        -rainfdates               <string>         Input rainfall time series dates ASCII file (Must be YYYYMMDD format)  (optional, off by default)
        -ndvi                     <group>          NDVI Time series 
        -ndvi.reduce              <string>         Method to reduce time series [cumul/max/amplitude] (mandatory, default value is cumul)
        -ndvi.reduce.cumul.day1   <int32>          Day 1  (mandatory, default value is 1)
        -ndvi.reduce.cumul.day2   <int32>          Day 2  (mandatory, default value is 31)
        -ndvi.reduce.cumul.month1 <int32>          Month 1  (mandatory, default value is 7)
        -ndvi.reduce.cumul.month2 <int32>          Month 2  (mandatory, default value is 10)
        -ndvitrend                <string> [pixel] Output image for NDVI trend (Contains [slope, pvalue, correlation])  [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (optional, off by default)
        -ndvilabel                <string> [pixel] Output image for NDVI label  [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is uint8) (optional, off by default)
        -rain                     <group>          Rainfall Time series 
        -rain.reduce              <string>         Method to reduce time series [cumul] (mandatory, default value is cumul)
        -rain.reduce.cumul.day1   <int32>          Day 1  (optional, on by default, default value is 1)
        -rain.reduce.cumul.day2   <int32>          Day 2  (optional, on by default, default value is 31)
        -rain.reduce.cumul.month1 <int32>          Month 1  (optional, on by default, default value is 6)
        -rain.reduce.cumul.month2 <int32>          Month 2  (optional, on by default, default value is 10)
        -residues                 <string> [pixel] Output image for NDVI residues  [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (optional, off by default)
        -restrend                 <string> [pixel] Output image for NDVI residues trend  [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (optional, off by default)
        -reslabel                 <string> [pixel] Output image for NDVI residues trend labels  [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is uint8) (optional, off by default)
        -factorslabel             <string> [pixel] Output image for factors labels  [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is uint8) (optional, off by default)
        -ndvicumul                <string> [pixel] Output image for ndvi year cumul  [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (optional, off by default)
        -rainfcumul               <string> [pixel] Output image for rainfall year cumul  [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (optional, off by default)
        -pearsoncoef              <string> [pixel] Output image for pearson coef  [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (optional, off by default)
        -ram                      <int32>          Available RAM (Mb)  (optional, off by default, default value is 128)
        -inxml                    <string>         Load otb application from xml file  (optional, off by default)
        -progress                 <boolean>        Report progress 
        -help                     <string list>    Display long help (empty list), or help for given parameters keys

Use -help param1 [... paramN] to see detailed documentation of those parameters.

Examples: 
None


```

TimeSeriesIndexTrend can be used as any OTB application (gui, command line, python, c++, ...).
To add the application in QGIS, just copy the file "_TimeSeriesIndexTrend.xml_" into the qgis/python/plugins/processing/algs/otb/description/5.0.0/ path.

## LandscapeStratification application

### Description

This application computes the landscape Stratification from an time series index (e.g. NDVI).
The implemented pipeline is described in [Bellón et al](http://dx.doi.org/10.3390/rs9060600).

![Pipeline](otbLandscapeStratification.png)


### How to use it

LandscapeStratification is an OTBApplication.

```
This is the LandscapeStratification (LandscapeStratification) application, version 6.2.0

Compute a landscape stratification metric
Parameters: 
        -progress <boolean>        Report progress 
        -help     <string list>    Display long help (empty list), or help for given parameters keys
MISSING -mask     <string>         Mask  (mandatory)
        -id       <string>         DatesFile  (optional, off by default)
        -cw       <float>          SpectralH  (optional, on by default, default value is 0.5)
        -sw       <float>          SpatialH  (optional, on by default, default value is 0.5)
MISSING -th       <float>          Threshold  (mandatory)
        -tmpdir   <string>         TempDir  (optional, off by default)
MISSING -ndvits   <string list>    Input NDVI time series images  (mandatory)
        -cbegin   <int32>          Component range begin  (optional, on by default, default value is 2)
        -cend     <int32>          Component range end  (optional, off by default)
MISSING -outvec   <string>         Output land units map (vector)  (mandatory)
        -ram      <int32>          Available RAM (Mb)  (optional, off by default, default value is 128)
        -inxml    <string>         Load otb application from xml file  (optional, off by default)

Use -help param1 [... paramN] to see detailed documentation of those parameters.
```

LandscapeStratification can be used as any OTB application (gui, command line, python, c++, ...).
To add the application in QGIS, just copy the file "_LandscapeStratification.xml_" into the qgis/python/plugins/processing/algs/otb/description/5.0.0/ path.

## License

Please see the license for legal issues on the use of the software (GNU Affero General Public License v3.0).

## Contact

Rémi Cresson (IRSTEA)

