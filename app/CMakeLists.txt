cmake_minimum_required (VERSION 2.8)

OTB_CREATE_APPLICATION(NAME           TimeSeriesIndexTrend
                       SOURCES        otbTimeSeriesIndexTrend.cxx
                       LINK_LIBRARIES OTBCommon)
                       
OTB_CREATE_APPLICATION(NAME           LandscapeStratificationMetric
                       SOURCES        otbLandscapeStratificationMetric.cxx
                       LINK_LIBRARIES OTBCommon)
                       