/*=========================================================================

  Program:   NDVITimeSeries
  Language:  C++

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.

  See LICENSE for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "itkFixedArray.h"
#include "itkObjectFactory.h"

// Elevation handler
#include "otbWrapperElevationParametersHandler.h"
#include "otbWrapperApplicationFactory.h"
#include "otbWrapperCompositeApplication.h"

// Application engine
#include "otbStandardFilterWatcher.h"
#include "itkFixedArray.h"

// LayerStack
#include "otbImageListToVectorImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
#include "otbImageList.h"
#include "otbMultiChannelExtractROI.h"

namespace otb
{

namespace Wrapper
{

class LandscapeStratificationMetric : public CompositeApplication
{
public:
  /** Standard class typedefs. */
  typedef LandscapeStratificationMetric              Self;
  typedef Application                         Superclass;
  typedef itk::SmartPointer<Self>             Pointer;
  typedef itk::SmartPointer<const Self>       ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);
  itkTypeMacro(LandscapeStratificationMetric, otb::Wrapper::CompositeApplication);

  /** Typedefs for image concatenation */
  typedef otb::ImageList<FloatImageType>                                               ImageListType;
  typedef ImageListToVectorImageFilter<ImageListType, FloatVectorImageType>            ListConcatenerFilterType;
  typedef MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,
      FloatImageType::PixelType>                                                       ExtractROIFilterType;
  typedef ObjectList<ExtractROIFilterType>                                             ExtractROIFilterListType;
  typedef otb::MultiChannelExtractROI<FloatVectorImageType::InternalPixelType,
      FloatVectorImageType::InternalPixelType>                                         ExtractFilterType;


private:


  void DoInit()
  {

  SetName("LandscapeStratificationMetric");
  SetDescription("Compute a landscape stratification metric");

  // Documentation
  SetDocLongDescription("This application implements the method of Bellon et al. (http://dx.doi.org/10.3390/rs9060600)");
  SetDocLimitations("None");
  SetDocAuthors("Remi Cresson");
  SetDocSeeAlso(" ");

  ClearApplications();

  // Add applications
  AddApplication("ImageTimeSeriesGapFilling",  "gapfillling" , "Image time series gap filling" );
  AddApplication("DimensionalityReduction"  ,  "pca"         , "Principal component analysis"  );

  // Fixed parameters
  GetInternalApplication("gapfillling")->SetParameterString("it"        , "linear" , false );
  GetInternalApplication("gapfillling")->SetParameterInt   ("comp"      , 1        , false );
  GetInternalApplication("pca"        )->SetParameterString("method"    , "pca"    , false );

  // Shared parameters
  ShareParameter("mask"  , "gapfillling.mask"      , "Mask"      , "Input time series mask"       );
  ShareParameter("id"    , "gapfillling.id"        , "DatesFile" , "Input time series dates file" );

  // Input NDVI time series images
  AddParameter(ParameterType_InputImageList,  "ndvits",   "Input NDVI time series images");

  // PCA ranges
  AddParameter(ParameterType_Int, "cbegin", "Component range begin" );
  SetMinimumParameterIntValue    ("cbegin", 1);
  SetDefaultParameterInt         ("cbegin", 2);
  MandatoryOff                   ("cbegin");
  AddParameter(ParameterType_Int, "cend"  , "Component range end"   );
  SetMinimumParameterIntValue    ("cend"  , 1);
  MandatoryOff                   ("cend");

  // Output image
  AddParameter(ParameterType_OutputImage, "out", "output metric image");

  // ram
  AddRAMParameter();
//  ShareParameter("ram" , "gapfillling.ram");
//  ShareParameter("ram" , "pca.ram");

  }


  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute()
  {

    // Get the NDVI time series input image list
    FloatVectorImageListType::Pointer inNDVIList = this->GetParameterImageList("ndvits");

    // Create one stack for input NDVI images list
    m_NDVIConcatener = ListConcatenerFilterType::New();
    m_NDVIImageList = ImageListType::New();
    m_NDVIExtractorList = ExtractROIFilterListType::New();

    // Split each input vector image into image
    // and generate an mono channel image list
    inNDVIList->GetNthElement(0)->UpdateOutputInformation();
    FloatVectorImageType::SizeType size = inNDVIList->GetNthElement(0)->GetLargestPossibleRegion().GetSize();
    for( unsigned int i=0; i<inNDVIList->Size(); i++ )
      {
      FloatVectorImageType::Pointer vectIm = inNDVIList->GetNthElement(i);
      vectIm->UpdateOutputInformation();
      if( size != vectIm->GetLargestPossibleRegion().GetSize() )
        {
        otbAppLogFATAL("Input NDVI image size number " << i << " mismatch");
        }

      for( unsigned int j=0; j<vectIm->GetNumberOfComponentsPerPixel(); j++)
        {
        ExtractROIFilterType::Pointer extractor = ExtractROIFilterType::New();
        extractor->SetInput( vectIm );
        extractor->SetChannel( j+1 );
        extractor->UpdateOutputInformation();
        m_NDVIExtractorList->PushBack( extractor );
        m_NDVIImageList->PushBack( extractor->GetOutput() );
        }
      }
    m_NDVIConcatener->SetInput( m_NDVIImageList );
    m_NDVIConcatener->UpdateOutputInformation();
    otbAppLogINFO("Size of the time series: " <<
        m_NDVIConcatener->GetOutput()->GetNumberOfComponentsPerPixel() );

    FloatVectorImageType::Pointer stack = m_NDVIConcatener->GetOutput();

    // Gapfilling
    GetInternalApplication("gapfillling")->SetParameterInputImage("in",
        static_cast<FloatVectorImageType*> (stack.GetPointer()));
    ExecuteInternal("gapfillling");

    // PCA
    GetInternalApplication("pca")->SetParameterInputImage("in",
      GetInternalApplication("gapfillling")->GetParameterOutputImage("out"));
    ExecuteInternal("pca");

    // Extract channels
    m_ExtractChannelsFilter = ExtractFilterType::New();
    unsigned int nbOfBands = m_NDVIConcatener->GetOutput()->GetNumberOfComponentsPerPixel();
    unsigned int chbegin = GetParameterInt("cbegin");
    if (chbegin > nbOfBands)
      {
      otbAppLogWARNING("Input parameter cbegin must be lower than the number of components. Using default.");
      chbegin = 2;
      }
    unsigned int chend = nbOfBands;
    if (HasValue("cend"))
      {
      unsigned int inputChEnd = GetParameterInt("cend");
      if (inputChEnd > nbOfBands)
        {
        otbAppLogWARNING("Input parameter cend is greater than the number of components. Using default.")
        }
      else if (inputChEnd < chbegin)
        {
        otbAppLogWARNING("Input parameter cend must be greater than cbegin. Using default.")
        }
      else
        {
        chend = inputChEnd;
        }
      }
    m_ExtractChannelsFilter = ExtractFilterType::New();

    m_ExtractChannelsFilter->SetFirstChannel(chbegin);
    m_ExtractChannelsFilter->SetLastChannel(chend);
    m_ExtractChannelsFilter->SetInput(static_cast<FloatVectorImageType*>(
        GetInternalApplication("pca")->GetParameterOutputImage("out")));

    SetParameterOutputImage("out" , m_ExtractChannelsFilter->GetOutput());

  }   // DOExecute()

  void AfterExecuteAndWriteOutputs()
  {
    // Nothing to do
  }

  // Layerstack
  ListConcatenerFilterType::Pointer           m_NDVIConcatener;
  ExtractROIFilterListType::Pointer           m_NDVIExtractorList;
  ImageListType::Pointer                      m_NDVIImageList;
  ExtractFilterType::Pointer                  m_ExtractChannelsFilter;

};
}
}

OTB_APPLICATION_EXPORT( otb::Wrapper::LandscapeStratificationMetric )
