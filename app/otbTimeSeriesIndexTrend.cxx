/*=========================================================================

  Program:   NDVITimeSeries
  Language:  C++

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.

  See LICENSE for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "itkFixedArray.h"
#include "itkObjectFactory.h"

// Elevation handler
#include "otbWrapperElevationParametersHandler.h"
#include "otbWrapperApplicationFactory.h"

// Application engine
#include "otbStandardFilterWatcher.h"
#include "itkFixedArray.h"

// Functors
#include "otbNDVITimeSeriesFunctor.h"
#include "otbFunctorImageFilter.h"

// LayerStack
#include "otbImageListToVectorImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
#include "otbImageList.h"
#include "otbMultiChannelExtractROI.h"

// Resample
#include "otbStreamingResampleImageFilter.h"
#include "itkLinearInterpolateImageFunction.h"
#include "itkNearestNeighborInterpolateImageFunction.h"

// Dates
#include "otbDates.h"

namespace otb
{

namespace Wrapper
{

class TimeSeriesIndexTrend : public Application
{
public:
  /** Standard class typedefs. */
  typedef TimeSeriesIndexTrend                Self;
  typedef Application                         Superclass;
  typedef itk::SmartPointer<Self>             Pointer;
  typedef itk::SmartPointer<const Self>       ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);
  itkTypeMacro(TimeSeriesIndexTrend, Application);

  /** Typedefs for image concatenation */
  typedef otb::ImageList<FloatImageType>  ImageListType;
  typedef ImageListToVectorImageFilter<ImageListType, FloatVectorImageType>            ListConcatenerFilterType;
  typedef MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,
      FloatImageType::PixelType>                                                       ExtractROIFilterType;
  typedef ObjectList<ExtractROIFilterType>                                             ExtractROIFilterListType;

  /** typedef for reduce operation */
  typedef otb::functor::TSAmplitudeReduceFunctor<FloatVectorImageType::PixelType,
      FloatVectorImageType::PixelType>                                                 TSAmplitudeReduceFunctorType;
  typedef otb::functor::TSCumulatedRangeReduceFunctor<FloatVectorImageType::PixelType,
      FloatVectorImageType::PixelType>                                                 TSCumulatedRangeReduceFunctorType;
  typedef otb::functor::TSMaxReduceFunctor<FloatVectorImageType::PixelType,
      FloatVectorImageType::PixelType>                                                 TSMaxReduceFunctorType;
  typedef otb::FunctorImageFilter<TSAmplitudeReduceFunctorType>                        TSAmplitudeReduceFilterType;
  typedef otb::FunctorImageFilter<TSCumulatedRangeReduceFunctorType>                   TSCumulatedRangeReduceFilterType;
  typedef otb::FunctorImageFilter<TSMaxReduceFunctorType>                              TSMaxReduceFilterType;

  /** typedef for trend computing (slope, p-value)  */
  typedef otb::functor::SlopeAndPValueFunctor<FloatVectorImageType::PixelType,
      FloatVectorImageType::PixelType>                                                 SlopeAndPValueFunctorType;
  typedef otb::FunctorImageFilter<SlopeAndPValueFunctorType>                           SlopeAndPValueFilterType;

  /** typedefs for trend labeling */
  typedef otb::functor::SlopeAndPValueLabelingFunctor<FloatVectorImageType::PixelType,
      UInt8VectorImageType::PixelType>                                                 SlopeAndPValueLabelFunctorType;
  typedef otb::FunctorImageFilter<SlopeAndPValueLabelFunctorType>                      SlopeAndPValueLabelFilterType;

  /* typedefs for dates*/
  typedef otb::dates::SingleDate                                                       DateType;

  /* typedefs for image resampling */
  typedef otb::StreamingResampleImageFilter<FloatVectorImageType,
      FloatVectorImageType>                                                            ResampleImageFilterType;
  typedef itk::LinearInterpolateImageFunction<FloatVectorImageType>                    LinearInterpolatorType;
  typedef itk::NearestNeighborInterpolateImageFunction<FloatVectorImageType>           NNInterpolatorType;

  /* typedefs for residues processing */
  typedef otb::functor::RainfallEstimatedNDVIResiduesFunctor<FloatVectorImageType::PixelType,
      FloatVectorImageType::PixelType>                                                 ResiduesFunctorType;
  typedef otb::FunctorImageFilter<ResiduesFunctorType>                                 ResiduesFilterType;

  /** typedefs for pearson correlation */
  typedef otb::functor::PearsonCorrelationCoefficientFunctor<FloatVectorImageType::PixelType,
      FloatVectorImageType::PixelType, FloatVectorImageType::PixelType>                PearsonCorrelationFunctorType;
  typedef otb::FunctorImageFilter<PearsonCorrelationFunctorType>                       PearsonCorrelationFilterType;

  /** typedefs for final labeling */
  typedef otb::functor::FactorsLabelingFunctor<FloatVectorImageType::PixelType,
      UInt8VectorImageType::PixelType>                                                 FactorsLabelingFunctorType;
  typedef otb::FunctorImageFilter<FactorsLabelingFunctorType>                          FactorsLabelingFilterType;

private:

  /*
   * Available methods to reduce time series
   */
  enum TS_REDUCE_METHOD
  {
    CUMUL,
    MAX,
    AMPLITUDE
  };

  void testFunctor()
  {
    SlopeAndPValueFunctorType f;

    FloatVectorImageType::PixelType p,y;
    unsigned int n = 10;
    p.SetSize(n);
    y.SetSize(n);
    for (unsigned int i = 1 ; i <= n ; i++)
      {
      p[i] = i*2;
      }
    std::cout << f(p) << std::endl;
  }

  void DoInit()
  {

    SetName("TimeSeriesIndexTrend");
    SetDescription("Compute the trend of the vegetation index time series");

    // Documentation
    SetDocLongDescription("This application implements the method of Leroux et al. (https://doi.org/10.1016/j.rse.2017.01.014)");
    SetDocLimitations("None");
    SetDocAuthors("Remi Cresson");
    SetDocSeeAlso(" ");

    // Input NDVI time series images
    AddParameter(ParameterType_InputImageList,  "ndvits",   "Input NDVI time series images");

    // Input NDVI time series dates
    AddParameter(ParameterType_InputFilename, "ndvidates", "Input NDVI time series dates ASCII file (Must be YYYYMMDD format)");

    // Input rainfall time series images
    AddParameter(ParameterType_InputImageList,  "rainfts",   "Input rainfall time series images");
    MandatoryOff("rainfts");

    // Input rainfall time series dates
    AddParameter(ParameterType_InputFilename, "rainfdates", "Input rainfall time series dates ASCII file (Must be YYYYMMDD format)");
    MandatoryOff("rainfdates");

    // Parameter group for NDVI time series
    AddParameter(ParameterType_Group, "ndvi", "NDVI Time series");

    // Method to reduce NDVI time series
    AddParameter(ParameterType_Choice, "ndvi.reduce", "Method to reduce NDVI time series");
    AddChoice("ndvi.reduce.cumul"    , "Cumul between two dates of the year, starting from ddstart/mmstart and during nbdays");
    AddChoice("ndvi.reduce.max"      , "Maximum in the year");
    AddChoice("ndvi.reduce.amplitude", "Amplitude in the year");
    AddParameter(ParameterType_Int, "ndvi.reduce.cumul.startday", "Start day");
    SetMinimumParameterIntValue    ("ndvi.reduce.cumul.startday", 1);
    SetMaximumParameterIntValue    ("ndvi.reduce.cumul.startday", 31);
    SetDefaultParameterInt         ("ndvi.reduce.cumul.startday", 1);
    MandatoryOff                   ("ndvi.reduce.cumul.startday");
    AddParameter(ParameterType_Int, "ndvi.reduce.cumul.startmonth", "Start month");
    SetMinimumParameterIntValue    ("ndvi.reduce.cumul.startmonth", 1);
    SetMaximumParameterIntValue    ("ndvi.reduce.cumul.startmonth", 12);
    SetDefaultParameterInt         ("ndvi.reduce.cumul.startmonth", 11); // november
    MandatoryOff                   ("ndvi.reduce.cumul.startmonth");
    AddParameter(ParameterType_Int, "ndvi.reduce.cumul.nbdays", "Cumul duration (number of days)");
    SetMinimumParameterIntValue    ("ndvi.reduce.cumul.nbdays", 1);
    SetMaximumParameterIntValue    ("ndvi.reduce.cumul.nbdays", 365);
    SetDefaultParameterInt         ("ndvi.reduce.cumul.nbdays", 150); // 150 days ~ 5 months
    MandatoryOff                   ("ndvi.reduce.cumul.nbdays");

    // Output NDVI (slope,p-value,corr) image
    AddParameter(ParameterType_OutputImage,  "ndvitrend",   "Output image for NDVI trend (Contains [slope, pvalue, correlation])");
    SetDefaultOutputPixelType("ndvitrend", ImagePixelType_float);
    MandatoryOff("ndvitrend");

    // Output NDVI label image
    AddParameter(ParameterType_OutputImage,  "ndvilabel",   "Output image for NDVI label");
    SetDefaultOutputPixelType("ndvilabel", ImagePixelType_uint8);
    MandatoryOff("ndvilabel");

    // Parameter group for rainfall time series
    AddParameter(ParameterType_Group, "rain", "Rainfall Time series");

    // Method to reduce raifall time series
    AddParameter(ParameterType_Choice, "rain.reduce", "Method to reduce rainfall time series");
    AddChoice("rain.reduce.cumul", "Cumul between two dates of the year, starting from ddstart/mmstart and during nbdays");
    AddParameter(ParameterType_Int, "rain.reduce.cumul.startday", "Start day");
    SetMinimumParameterIntValue    ("rain.reduce.cumul.startday", 1);
    SetMaximumParameterIntValue    ("rain.reduce.cumul.startday", 31);
    SetDefaultParameterInt         ("rain.reduce.cumul.startday", 1);
    MandatoryOff                   ("rain.reduce.cumul.startday");
    AddParameter(ParameterType_Int, "rain.reduce.cumul.startmonth", "Start month");
    SetMinimumParameterIntValue    ("rain.reduce.cumul.startmonth", 1);
    SetMaximumParameterIntValue    ("rain.reduce.cumul.startmonth", 12);
    SetDefaultParameterInt         ("rain.reduce.cumul.startmonth", 11); // november
    MandatoryOff                   ("rain.reduce.cumul.startmonth");
    AddParameter(ParameterType_Int, "rain.reduce.cumul.nbdays", "Cumul duration (number of days)");
    SetMinimumParameterIntValue    ("rain.reduce.cumul.nbdays", 1);
    SetMaximumParameterIntValue    ("rain.reduce.cumul.nbdays", 365);
    SetDefaultParameterInt         ("rain.reduce.cumul.nbdays", 150); // 150 days ~ 5 months
    MandatoryOff                   ("rain.reduce.cumul.nbdays");

    // Method to resample rainfall time series
    AddParameter(ParameterType_Choice, "rain.interpolator", "Interpolation");
    MandatoryOff("rain.interpolator");
    AddChoice("rain.interpolator.linear", "Linear interpolation");
    AddChoice("rain.interpolator.nn",     "Nearest Neighbor interpolation");

    // Output residues
    AddParameter(ParameterType_OutputImage,  "residues",   "Output image for NDVI residues");
    SetDefaultOutputPixelType("residues", ImagePixelType_float);
    MandatoryOff             ("residues");

    // Output residues trend
    AddParameter(ParameterType_OutputImage,  "restrend",   "Output image for NDVI residues trend");
    SetDefaultOutputPixelType("restrend", ImagePixelType_float);
    MandatoryOff             ("restrend");

    // Output residues trend labels
    AddParameter(ParameterType_OutputImage,  "reslabel",   "Output image for NDVI residues trend labels");
    SetDefaultOutputPixelType("reslabel", ImagePixelType_uint8);
    MandatoryOff             ("reslabel");

    // Output factors labels
    AddParameter(ParameterType_OutputImage,  "factorslabel",   "Output image for factors labels");
    SetDefaultOutputPixelType("factorslabel", ImagePixelType_uint8);
    MandatoryOff             ("factorslabel");

    // Output NDVI (year cumul)
    AddParameter(ParameterType_OutputImage,  "ndvicumul",   "Output image for ndvi year cumul");
    SetDefaultOutputPixelType("ndvicumul", ImagePixelType_float);
    MandatoryOff             ("ndvicumul");

    // Output rainf (year cumul)
    AddParameter(ParameterType_OutputImage,  "rainfcumul",   "Output image for rainfall year cumul");
    SetDefaultOutputPixelType("rainfcumul", ImagePixelType_float);
    MandatoryOff             ("rainfcumul");

    // Output pearson coef
    AddParameter(ParameterType_OutputImage,  "pearsoncoef",   "Output image for pearson coef");
    SetDefaultOutputPixelType("pearsoncoef", ImagePixelType_float);
    MandatoryOff             ("pearsoncoef");

    AddRAMParameter();

  }

  /*
   * Retrieve the dates (numeric) from the input dates (string)
   * Input dates (string) must be formated using the following pattern: YYYYMMDD
   */
  std::vector<DateType> GetTimeSeriesDates(std::string key){

    std::vector<DateType> dates = otb::dates::GetDatesFromFile(key);
    unsigned int n = dates.size() - 1;
    otbAppLogINFO("Using " << dates.size() << " input dates from "
        << dates[0].year << "/" << dates[0].month << "/" << dates[0].day << " to "
        << dates[n].year << "/" << dates[n].month << "/" << dates[n].day );

    return dates;
  }

  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute()
  {

    // Get the NDVI time series dates
    std::vector<DateType> ndviDates = GetTimeSeriesDates(GetParameterAsString("ndvidates"));

    // Get the NDVI time series input image list
    FloatVectorImageListType::Pointer inNDVIList = this->GetParameterImageList("ndvits");

    // Create one stack for input NDVI images list
    m_NDVIConcatener = ListConcatenerFilterType::New();
    m_NDVIImageList = ImageListType::New();
    m_NDVIExtractorList = ExtractROIFilterListType::New();
    // Split each input vector image into image
    // and generate an mono channel image list
    inNDVIList->GetNthElement(0)->UpdateOutputInformation();
    FloatVectorImageType::SizeType size = inNDVIList->GetNthElement(0)->GetLargestPossibleRegion().GetSize();
    for( unsigned int i=0; i<inNDVIList->Size(); i++ )
      {
      FloatVectorImageType::Pointer vectIm = inNDVIList->GetNthElement(i);
      vectIm->UpdateOutputInformation();
      if( size != vectIm->GetLargestPossibleRegion().GetSize() )
        {
        otbAppLogFATAL("Input NDVI image size number " << i << " mismatch");
        }

      for( unsigned int j=0; j<vectIm->GetNumberOfComponentsPerPixel(); j++)
        {
        ExtractROIFilterType::Pointer extractor = ExtractROIFilterType::New();
        extractor->SetInput( vectIm );
        extractor->SetChannel( j+1 );
        extractor->UpdateOutputInformation();
        m_NDVIExtractorList->PushBack( extractor );
        m_NDVIImageList->PushBack( extractor->GetOutput() );
        }
      }
    m_NDVIConcatener->SetInput( m_NDVIImageList );
    m_NDVIConcatener->UpdateOutputInformation();

    if (m_NDVIConcatener->GetOutput()->GetNumberOfComponentsPerPixel() != ndviDates.size())
      otbAppLogFATAL("Number of NDVI images and number of dates are different! There is "
          << (m_NDVIConcatener->GetOutput()->GetNumberOfComponentsPerPixel()) << " images "
          << "and " << ndviDates.size() << " dates.");

    // Get the detection method
    FloatVectorImageType::Pointer reducedNDVI;
    if (GetParameterInt("ndvi.reduce") == CUMUL)
      {
      m_NDVICumulReduceFilter = TSCumulatedRangeReduceFilterType::New();
      m_NDVICumulReduceFilter->SetInput(m_NDVIConcatener->GetOutput());
      m_NDVICumulReduceFilter->GetModifiableFunctor().SetDates(ndviDates);
      m_NDVICumulReduceFilter->GetModifiableFunctor().SetStartDay(GetParameterInt  ("ndvi.reduce.cumul.startday"));
      m_NDVICumulReduceFilter->GetModifiableFunctor().SetStartMonth(GetParameterInt("ndvi.reduce.cumul.startmonth"));
      m_NDVICumulReduceFilter->GetModifiableFunctor().SetNbOfDays(GetParameterInt  ("ndvi.reduce.cumul.nbdays"));
      reducedNDVI = m_NDVICumulReduceFilter->GetOutput();
      }
    else if (GetParameterInt("ndvi.reduce") == MAX)
      {
      m_NDVIMaxReduceFilter = TSMaxReduceFilterType::New();
      m_NDVIMaxReduceFilter->SetInput(m_NDVIConcatener->GetOutput());
      m_NDVIMaxReduceFilter->GetModifiableFunctor().SetDates(ndviDates);
      reducedNDVI = m_NDVIMaxReduceFilter->GetOutput();
      }
    else if (GetParameterInt("ndvi.reduce") == AMPLITUDE)
      {
      m_NDVIAmplitudeReduceFilter = TSAmplitudeReduceFilterType::New();
      m_NDVIAmplitudeReduceFilter->SetInput(m_NDVIConcatener->GetOutput());
      m_NDVIAmplitudeReduceFilter->GetModifiableFunctor().SetDates(ndviDates);
      reducedNDVI = m_NDVIAmplitudeReduceFilter->GetOutput();
      }
    else
      {
      otbAppLogFATAL("Unknown reduction method.")
      }

    // NDVI Trend
    m_NDVITimeSeriesRegressionFilter = SlopeAndPValueFilterType::New();
    m_NDVITimeSeriesRegressionFilter->SetInput(reducedNDVI);
    m_NDVITimeSeriesRegressionFilter->GetModifiableFunctor().SetDates(ndviDates);

    // Write reduced ndvi
    if (HasValue("ndvicumul"))
      {
      SetParameterOutputImage("ndvicumul", reducedNDVI);
      }

    // Output NDVI trend image
    if (HasValue("ndvitrend"))
      {
      SetParameterOutputImage("ndvitrend", m_NDVITimeSeriesRegressionFilter->GetOutput());
      }

    // Output NDVI trend labels
    if (HasValue("ndvilabel"))
      {
      m_NDVITimeSeriesLabelFilter = SlopeAndPValueLabelFilterType::New();
      m_NDVITimeSeriesLabelFilter->SetInput(m_NDVITimeSeriesRegressionFilter->GetOutput());
      SetParameterOutputImage("ndvilabel", m_NDVITimeSeriesLabelFilter->GetOutput());
      }

    // Check rainfall mandatory parameters
    if (HasValue("rainfdates") && HasValue("rainfts"))
      {

        // Get the rainfall time series dates
        std::vector<DateType> rainDates = GetTimeSeriesDates(GetParameterAsString("rainfdates"));

        // Get the rainfall time series input images list
        FloatVectorImageListType::Pointer inRFList = this->GetParameterImageList("rainfts");

        // Create one stack for input RF images list
        m_RFConcatener = ListConcatenerFilterType::New();
        m_RFImageList = ImageListType::New();
        m_RFExtractorList = ExtractROIFilterListType::New();

        // Split each input vector image into image
        // and generate an mono channel image list
        inRFList->GetNthElement(0)->UpdateOutputInformation();
        FloatVectorImageType::SizeType rfSize = inRFList->GetNthElement(0)->GetLargestPossibleRegion().GetSize();
        for( unsigned int i=0; i<inRFList->Size(); i++ )
          {
            FloatVectorImageType::Pointer vectIm = inRFList->GetNthElement(i);
            vectIm->UpdateOutputInformation();
            if( rfSize != vectIm->GetLargestPossibleRegion().GetSize() )
              {
                otbAppLogFATAL("Input NDVI image size number " << i << " mismatch");
              }

            for( unsigned int j=0; j<vectIm->GetNumberOfComponentsPerPixel(); j++)
              {
                ExtractROIFilterType::Pointer extractor = ExtractROIFilterType::New();
                extractor->SetInput( vectIm );
                extractor->SetChannel( j+1 );
                extractor->UpdateOutputInformation();
                m_RFExtractorList->PushBack( extractor );
                m_RFImageList->PushBack( extractor->GetOutput() );
              }
          }
        m_RFConcatener->SetInput( m_RFImageList );
        m_RFConcatener->UpdateOutputInformation();

        if (m_RFConcatener->GetOutput()->GetNumberOfComponentsPerPixel() != rainDates.size())
          otbAppLogFATAL("Number of rainfall images and number of dates are different! There is "
              << (m_RFConcatener->GetOutput()->GetNumberOfComponentsPerPixel()) << " images "
              << "and " << rainDates.size() << " dates.");
        // Check dates consistency
        int firstYearForNDVI = ndviDates[0].year;
        int lastYearForNDVI = ndviDates[ndviDates.size()-1].year;
        int firstYearForRF = rainDates[0].year;
        int lastYearForRF = rainDates[rainDates.size()-1].year;
        if (firstYearForNDVI != firstYearForRF)
          {
            otbAppLogFATAL("NDVI time series and rainfall time series must start the same year!"
                << " NDVI start year " << firstYearForNDVI
                << "and rainfall start year " << firstYearForRF << ".");
          }
        if (lastYearForNDVI != lastYearForRF)
          {
            otbAppLogFATAL("NDVI time series and rainfall time series must end the same year!"
                << " NDVI last year " << lastYearForNDVI
                << "and rainfall last year " << lastYearForRF << ".");
          }
        // Computes new dates of the reduced time series
        std::vector<DateType> reducedTimeSeriesDates;
        for (int year = firstYearForNDVI; year <= lastYearForNDVI; year++)
          {
          DateType newDate(year,1,1);
          reducedTimeSeriesDates.push_back(newDate);
          }

        // Reduce rainfall
        m_RFCumulReduceFilter = TSCumulatedRangeReduceFilterType::New();
        m_RFCumulReduceFilter->SetInput(m_RFConcatener->GetOutput());
        m_RFCumulReduceFilter->GetModifiableFunctor().SetDates(rainDates);
        m_RFCumulReduceFilter->GetModifiableFunctor().SetStartDay(GetParameterInt  ("rain.reduce.cumul.startday"));
        m_RFCumulReduceFilter->GetModifiableFunctor().SetStartMonth(GetParameterInt("rain.reduce.cumul.startmonth"));
        m_RFCumulReduceFilter->GetModifiableFunctor().SetNbOfDays(GetParameterInt  ("rain.reduce.cumul.nbdays"));
        // Resample rainfall
        LinearInterpolatorType::Pointer linInterpolator = LinearInterpolatorType::New();
        NNInterpolatorType::Pointer nnInterpolator = NNInterpolatorType::New();
        m_ResampleFilter = ResampleImageFilterType::New();
        m_ResampleFilter->SetInput(m_RFCumulReduceFilter->GetOutput());
        m_ResampleFilter->SetOutputOrigin(m_NDVIConcatener->GetOutput()->GetOrigin());
        m_ResampleFilter->SetOutputSpacing(m_NDVIConcatener->GetOutput()->GetSignedSpacing());
        m_ResampleFilter->SetOutputSize(m_NDVIConcatener->GetOutput()->GetLargestPossibleRegion().GetSize());
        if (HasValue("rain.interpolator") == 0)
          {
          m_ResampleFilter->SetInterpolator(linInterpolator);
          }
        else if (HasValue("rain.interpolator") == 1)
          {
          m_ResampleFilter->SetInterpolator(nnInterpolator);
          }
        else
          {
          otbAppLogFATAL("Unknown interpolator type!");
          }

        // Write reduced rainfall
        if (HasValue("rainfcumul"))
          {
          SetParameterOutputImage("rainfcumul", m_ResampleFilter->GetOutput());
          }
        // Compute residues
        m_ResiduesFilter = ResiduesFilterType::New();
        m_ResiduesFilter->SetInput1(reducedNDVI);
        m_ResiduesFilter->SetInput2(m_ResampleFilter->GetOutput());

        // Output residues
        if (HasValue("residues"))
          {
            SetParameterOutputImage("residues", m_ResiduesFilter->GetOutput());
          }
        // Regression (residues)
        m_NDVIResiduesRegressionFilter = SlopeAndPValueFilterType::New();
        m_NDVIResiduesRegressionFilter->SetInput(m_ResiduesFilter->GetOutput());
        m_NDVIResiduesRegressionFilter->GetModifiableFunctor().SetDates(reducedTimeSeriesDates);

        // Output residues trend
        if (HasValue("restrend"))
          {
            SetParameterOutputImage("restrend", m_NDVIResiduesRegressionFilter->GetOutput());
          }
        // Output residues trend labels
        if (HasValue("reslabel"))
          {
            m_NDVIResiduesLabelFilter = SlopeAndPValueLabelFilterType::New();
            m_NDVIResiduesLabelFilter->SetInput(m_NDVIResiduesRegressionFilter->GetOutput());
            SetParameterOutputImage("reslabel", m_NDVIResiduesLabelFilter->GetOutput());
          }
        // Correlation between NDVI and Rainfall
        m_CorrelationFilter = PearsonCorrelationFilterType::New();
        m_CorrelationFilter->SetInput1(reducedNDVI);
        m_CorrelationFilter->SetInput2(m_ResampleFilter->GetOutput());

        if (HasValue("pearsoncoef"))
          {
          SetParameterOutputImage("pearsoncoef", m_CorrelationFilter->GetOutput());
          }
        // Factors labeling
        m_FactorsLabelFilter = FactorsLabelingFilterType::New();
        m_FactorsLabelFilter->SetInput1(m_NDVITimeSeriesRegressionFilter->GetOutput());
        m_FactorsLabelFilter->SetInput2(m_NDVIResiduesRegressionFilter->GetOutput());
        m_FactorsLabelFilter->SetInput3(m_CorrelationFilter->GetOutput());

        // Write factors image
        if (HasValue("factorslabel"))
          {
            SetParameterOutputImage("factorslabel", m_FactorsLabelFilter->GetOutput());
          }
      }
    else
      {
        if (HasValue("factorslabel") || HasValue("reslabel") || HasValue("restrend") || HasValue("residues"))
          {
            otbAppLogFATAL("Please set input rainfall data to process indices.")
          }

        otbAppLogINFO("Rainfall data not set. Skipping rainfall based indices.")
      }

  }   // DOExecute()

  void AfterExecuteAndWriteOutputs()
  {
    // Nothing to do
  }

  // Layerstack
  ListConcatenerFilterType::Pointer           m_NDVIConcatener;
  ExtractROIFilterListType::Pointer           m_NDVIExtractorList;
  ImageListType::Pointer                      m_NDVIImageList;
  ListConcatenerFilterType::Pointer           m_RFConcatener;
  ExtractROIFilterListType::Pointer           m_RFExtractorList;
  ImageListType::Pointer                      m_RFImageList;

  // Reduce filters
  TSAmplitudeReduceFilterType::Pointer        m_NDVIAmplitudeReduceFilter;
  TSCumulatedRangeReduceFilterType::Pointer   m_NDVICumulReduceFilter;
  TSMaxReduceFilterType::Pointer              m_NDVIMaxReduceFilter;
  TSCumulatedRangeReduceFilterType::Pointer   m_RFCumulReduceFilter;

  // Trend filters
  SlopeAndPValueFilterType::Pointer           m_NDVITimeSeriesRegressionFilter;
  SlopeAndPValueFilterType::Pointer           m_NDVIResiduesRegressionFilter;

  // Label filters
  SlopeAndPValueLabelFilterType::Pointer      m_NDVITimeSeriesLabelFilter;
  SlopeAndPValueLabelFilterType::Pointer      m_NDVIResiduesLabelFilter;

  // Resample filter
  ResampleImageFilterType::Pointer            m_ResampleFilter;

  // Filter for residues
  ResiduesFilterType::Pointer                 m_ResiduesFilter;

  // Pearson correlation filter
  PearsonCorrelationFilterType::Pointer       m_CorrelationFilter;

  // Final labeling filter
  FactorsLabelingFilterType::Pointer          m_FactorsLabelFilter;
};
}
}

OTB_APPLICATION_EXPORT( otb::Wrapper::TimeSeriesIndexTrend )
